# Test task #3 by https://maxpfrontend.ru

## Installing

Run [backend](https://github.com/maxfarseer/backend-tz3)

Clone repository `git clone`

Create `.env` file and pass `REACT_APP_API_URL` variable with your backend url and `REACT_APP_GL_CLIENT` with your Google Client ID

```
REACT_APP_API_URL=http://localhost:5000/api/v1
REACT_APP_GL_CLIENT=000000000000-23jpt5237l5aqi0kv112h2n50jcj7e1q.apps.googleusercontent.com
```

```
yarn install
yarn start
```
