import jwt from 'jsonwebtoken'

export const decodeUser = (authToken, accessToken) => {
  const { name, email, picture } = jwt.decode(authToken)
  const { id } = jwt.decode(accessToken)

  return { id, name, email, picture }
}

export const trimText = (text, length) =>
  text.length > length ? `${text.substring(0, length)} ...` : text
