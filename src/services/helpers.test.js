import { trimText } from './helpers'

describe('helpers tests', () => {
  it('should trim text and add three dots', () => {
    expect(trimText('Lorem ipsum dolor.', 4)).toEqual('Lore ...')
  })
})
