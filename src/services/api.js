import axios from 'axios'
import { Cookies } from 'react-cookie'

const cookies = new Cookies()
const user = cookies.get('nwltr_user')
const token = user && user.token

export default axios.create({
  baseURL: process.env.REACT_APP_API_URL,
  headers: {
    'x-access-token': token ? token : ''
  }
})
