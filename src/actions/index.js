import API from '../services/api'
import {
  SIGN_IN_REQUEST,
  SIGN_IN_SUCCESS,
  SIGN_UP_REQUEST,
  SIGN_UP_SUCCESS,
  INIT_START,
  INIT_SUCCESS,
  SIGN_OUT_REQUEST,
  SIGN_OUT_SUCCESS,
  ADD_ARTICLE_REQUEST,
  ADD_ARTICLE_SUCCESS,
  FETCH_ARTICLES_REQUEST,
  FETCH_ARTICLES_SUCCESS,
  DELETE_ARTICLE_SUCCESS,
  FETCH_ARTICLE_REQUEST,
  FETCH_ARTICLE_SUCCESS,
  EDIT_ARTICLE_SUCCESS,
  CLEAR_ARTICLE
} from '../constants'
import { decodeUser } from '../services/helpers'
import { history } from '../history'
import jwt from 'jsonwebtoken'
import { Cookies } from 'react-cookie'
import { notification, message } from 'antd'

const cookies = new Cookies()

function showError() {
  notification.error({
    message: 'Error',
    description: 'Something went wrong'
  })
}

export function appInit() {
  return async (dispatch) => {
    try {
      dispatch({
        type: INIT_START
      })

      dispatch(userInit())

      await window.gapi.load('auth2', () => {
        window.gapi.auth2.init({
          client_id: process.env.REACT_APP_GL_CLIENT
        })
      })

      dispatch({
        type: INIT_SUCCESS
      })
    } catch (error) {
      showError()
    }
  }
}

export function userInit() {
  return (dispatch) => {
    const user = cookies.get('nwltr_user')

    user &&
      dispatch({
        type: SIGN_IN_SUCCESS,
        payload: { user }
      })
  }
}

export function signUp(user) {
  return async (dispatch) => {
    dispatch({
      type: SIGN_UP_REQUEST
    })

    try {
      const {
        data: { token }
      } = await API.post('/users', { ...user })

      dispatch({
        type: SIGN_UP_SUCCESS,
        payload: { token }
      })

      history.push('/auth/sign-in')
    } catch (error) {
      showError()
    }
  }
}

export function signIn(payload) {
  return async (dispatch) => {
    dispatch({
      type: SIGN_IN_REQUEST
    })

    try {
      const {
        data: { token }
      } = await API.post('/auth', { ...payload })

      const { id, exp } = jwt.decode(token)
      const {
        data: { user }
      } = await API.get(`/users/${id}`, {
        headers: {
          'x-access-token': token
        }
      })

      cookies.set(
        'nwltr_user',
        { ...user, token },
        {
          expires: new Date(exp * 1000)
        }
      )

      dispatch({
        type: SIGN_IN_SUCCESS,
        payload: { user }
      })

      history.push('/')
    } catch (error) {
      showError()
    }
  }
}

export function googleSignIn() {
  return async (dispatch) => {
    dispatch({
      type: SIGN_IN_REQUEST
    })

    try {
      const auth2 = window.gapi.auth2.getAuthInstance()
      const googleUser = await auth2.signIn()
      const authToken = googleUser.getAuthResponse().id_token

      const {
        data: { token: accessToken }
      } = await API.post('/auth/google', { token: authToken })

      const decodedUser = decodeUser(authToken, accessToken)
      const user = { ...decodedUser, _id: decodedUser.id }
      const { exp } = jwt.decode(accessToken)

      cookies.set(
        'nwltr_user',
        { ...user, token: accessToken },
        {
          expires: new Date(exp * 1000)
        }
      )

      dispatch({
        type: SIGN_IN_SUCCESS,
        payload: { user }
      })

      history.push('/')
    } catch (error) {
      showError()
    }
  }
}

export function signOut() {
  return async (dispatch) => {
    dispatch({
      type: SIGN_OUT_REQUEST
    })

    try {
      const auth2 = window.gapi.auth2.getAuthInstance()
      await auth2.signOut()

      cookies.remove('nwltr_user')

      dispatch({
        type: SIGN_OUT_SUCCESS
      })
    } catch (error) {
      showError()
    }
  }
}

export function addArticle(newArticle) {
  return async (dispatch) => {
    dispatch({
      type: ADD_ARTICLE_REQUEST
    })

    try {
      const {
        data: { feed }
      } = await API.post('/feeds', { ...newArticle })

      dispatch({
        type: ADD_ARTICLE_SUCCESS,
        payload: { feed }
      })

      message.success('Article added!', 3)
    } catch (error) {
      showError()
    }
  }
}

export function deleteArticle(id) {
  return async (dispatch) => {
    try {
      const {
        data: { _id }
      } = await API.delete(`/feeds/${id}`)

      dispatch({
        type: DELETE_ARTICLE_SUCCESS,
        payload: { _id }
      })
    } catch (error) {
      showError()
    }
  }
}

export function editArticle(id, newArticle) {
  return async (dispatch) => {
    try {
      const { data } = await API.put(`/feeds/${id}`, { ...newArticle })

      dispatch({
        type: EDIT_ARTICLE_SUCCESS,
        payload: data
      })

      message.success('Article edited!', 3)
      history.push(`/articles/${id}`)
    } catch (error) {
      showError()
    }
  }
}

export function clearArticle() {
  return {
    type: CLEAR_ARTICLE
  }
}

export function fetchArticles() {
  return async (dispatch) => {
    dispatch({
      type: FETCH_ARTICLES_REQUEST
    })

    try {
      const { data } = await API.get('/feeds')

      dispatch({
        type: FETCH_ARTICLES_SUCCESS,
        payload: data
      })
    } catch (error) {
      showError()
    }
  }
}

export function fetchArticle(id) {
  return async (dispatch) => {
    dispatch({
      type: FETCH_ARTICLE_REQUEST
    })

    try {
      const { data } = await API.get(`/feeds/${id}`)

      dispatch({
        type: FETCH_ARTICLE_SUCCESS,
        payload: data
      })
    } catch (error) {
      showError()
    }
  }
}
