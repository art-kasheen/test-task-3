import styled from 'styled-components'

export const SmallContainer = styled.div`
  max-width: 560px;
  margin: 0 auto;
  padding-left: 15px;
  padding-right: 15px;
`

export const Container = styled.div`
  max-width: 960px;
  margin: 0 auto;
  padding-left: 15px;
  padding-right: 15px;
`
