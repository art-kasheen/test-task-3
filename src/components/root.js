import React, { useEffect } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { Switch, Route, Redirect } from 'react-router-dom'
import Header from './header'
import { appInit } from '../actions'
import Auth from './routes/auth'
import Articles from './routes/articles'
import NotFound from '../components/not-found'
import Loader from './loader'

const Root = ({ appInit, isAuthenticated, loading, initialized }) => {
  useEffect(() => {
    appInit()
    // eslint-disable-next-line
  }, [])

  if (loading || !initialized) return <Loader size="large" />

  return (
    <div>
      <Header />
      <Switch>
        <Redirect exact from="/" to="/articles" />
        <Route path="/auth" component={Auth} />
        <Route
          path="/articles"
          render={() => <Articles isAuthenticated={isAuthenticated} />}
        />
        <Route component={NotFound} />
      </Switch>
    </div>
  )
}

Root.propTypes = {
  appInit: PropTypes.func.isRequired,
  loading: PropTypes.bool.isRequired,
  initialized: PropTypes.bool.isRequired,
  isAuthenticated: PropTypes.bool.isRequired
}

export default connect(
  (state) => ({
    isAuthenticated: !!state.auth.user,
    loading: state.common.loading,
    initialized: state.common.initialized
  }),
  { appInit }
)(Root)
