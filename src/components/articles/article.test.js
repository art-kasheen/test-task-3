import React from 'react'
import { shallow, mount } from 'enzyme'
import { Article } from './article'
import Loader from '../loader'
import articles from '../../mocks/articles'

describe('article component', () => {
  const defaultProps = {
    loading: false,
    receivedAt: 1,
    fetchArticle: () => {},
    deleteArticle: () => {},
    clearArticle: () => {},
    match: { params: { id: 1 } },
    article: articles.feeds[0],
    user: { _id: '5d4acd6911420398763760ea', displayName: 'user1' }
  }

  it('should fetch article on load', () => {
    const handleFetch = jest.fn()
    const props = {
      ...defaultProps,
      fetchArticle: handleFetch
    }

    mount(<Article {...props} />)
    expect(handleFetch).toBeCalled()
  })

  it('should call clear article on unmount', () => {
    const handleClear = jest.fn()
    const props = {
      ...defaultProps,
      clearArticle: handleClear
    }

    mount(<Article {...props} />).unmount()
    expect(handleClear).toBeCalled()
  })

  it('should render loader', () => {
    const props = {
      ...defaultProps,
      loading: true
    }
    const wrapper = shallow(<Article {...props} />)

    expect(wrapper.contains(<Loader />))
  })

  it('should render title', () => {
    const wrapper = shallow(<Article {...defaultProps} />)

    expect(wrapper.find('[data-test="article-title"]').text()).toEqual(
      defaultProps.article.title
    )
  })

  it('should render content', () => {
    const wrapper = shallow(<Article {...defaultProps} />)

    expect(wrapper.find('[data-test="article-content"]').text()).toEqual(
      defaultProps.article.content
    )
  })

  it('should render actions on editable mode', () => {
    const wrapper = shallow(<Article {...defaultProps} />)

    expect(wrapper.find('[data-test="article-actions"]').length).toEqual(1)
  })

  it('should not render actions not for the author', () => {
    const props = {
      ...defaultProps,
      user: { _id: '5d4acd691142039876374533', displayName: 'user2' }
    }
    const wrapper = shallow(<Article {...props} />)

    expect(wrapper.find('[data-test="article-actions"]').length).toEqual(0)
  })
})
