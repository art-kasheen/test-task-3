import React, { useEffect } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { fetchArticles, deleteArticle } from '../../actions'
import Loader from '../loader'
import ArticleCard from './article-card'
import { Row, Col } from 'antd'

export const ArticleList = ({
  user,
  fetchArticles,
  articles,
  loading,
  receivedAt,
  deleteArticle
}) => {
  useEffect(() => {
    fetchArticles()
    // eslint-disable-next-line
  }, [])

  if (receivedAt && !loading && !articles.length)
    return <p data-test="article-empty">No articles...</p>
  if (loading) return <Loader />

  const items = articles.reverse().map((article) => (
    <Col md={12} sm={24} key={article._id} style={{ marginBottom: '16px' }}>
      <ArticleCard
        data-test="article-card"
        editable={user ? article.creator._id === user._id : false}
        article={article}
        deleteArticle={deleteArticle}
      />
    </Col>
  ))

  return <Row gutter={16}>{items}</Row>
}

ArticleList.propTypes = {
  user: PropTypes.object,
  articles: PropTypes.array.isRequired,
  loading: PropTypes.bool.isRequired,
  receivedAt: PropTypes.number,
  deleteArticle: PropTypes.func.isRequired
}

export default connect(
  (state) => ({
    user: state.auth.user,
    articles: state.articles.data,
    loading: state.articles.loading,
    receivedAt: state.articles.receivedAt,
    fetchArticles: PropTypes.func.isRequired
  }),
  { fetchArticles, deleteArticle }
)(ArticleList)
