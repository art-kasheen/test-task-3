import React from 'react'
import { shallow, mount } from 'enzyme'
import { ArticleCard } from './article-card'
import articles from '../../mocks/articles'
import { BrowserRouter as Router } from 'react-router-dom'

describe('article card component', () => {
  it('should render actions in editable mode', () => {
    const props = {
      editable: true,
      article: articles.feeds[0],
      deleteArticle: () => {}
    }

    const wrapper = shallow(<ArticleCard {...props} />)

    expect(
      wrapper.find('[data-test="article-card"]').prop('extra')
    ).not.toBeFalsy()
  })

  it('should render meta data', () => {
    const props = {
      editable: false,
      article: articles.feeds[0],
      deleteArticle: () => {}
    }

    const wrapper = shallow(<ArticleCard {...props} />)

    expect(wrapper.find('[data-test="article-meta"]').text()).toBe(
      '8/7/2019 by user1'
    )
  })

  it('should delete article', () => {
    const handleClick = jest.fn()
    const props = {
      editable: true,
      article: articles.feeds[0],
      deleteArticle: handleClick
    }
    const wrapper = mount(
      <Router>
        <ArticleCard {...props} />
      </Router>
    )

    wrapper.find('[data-test="article-delete"]').simulate('click')
    wrapper
      .find('.ant-popover-buttons Button')
      .at(1)
      .simulate('click')

    expect(handleClick).toHaveBeenCalled()
  })
})
