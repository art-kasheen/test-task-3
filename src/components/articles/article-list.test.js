import React from 'react'
import { shallow } from 'enzyme'
import { ArticleList } from './article-list'
import Loader from '../loader'
import articles from '../../mocks/articles'

describe('article list component', () => {
  it('should render loader', () => {
    const props = {
      articles: [],
      loading: true,
      deleteArticle: () => {}
    }

    const wrapper = shallow(<ArticleList {...props} />)

    expect(wrapper.contains(<Loader />)).toBeTruthy()
  })

  it('should render list', () => {
    const props = {
      articles: articles.feeds,
      loading: false,
      deleteArticle: () => {}
    }

    const wrapper = shallow(<ArticleList {...props} />)

    expect(wrapper.find('[data-test="article-card"]').length).toEqual(
      articles.feeds.length
    )
  })

  it('should render message when no articles', () => {
    const props = {
      articles: [],
      loading: false,
      receivedAt: Date.now(),
      deleteArticle: () => {}
    }

    const wrapper = shallow(<ArticleList {...props} />)

    expect(wrapper.find('[data-test="article-empty"]').length).toEqual(1)
  })
})
