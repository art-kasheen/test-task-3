import React from 'react'
import PropTypes from 'prop-types'
import { Card, Icon, Popconfirm } from 'antd'
import { Link } from 'react-router-dom'
import styled from 'styled-components'
import { trimText } from '../../services/helpers'

export const ArticleCard = ({ editable, article, deleteArticle }) => {
  const title = <Link to={`/articles/${article._id}`}>{article.title}</Link>
  const date = new Date(article.createDate).toLocaleDateString('en-EN')
  const {
    content,
    creator: { displayName: author }
  } = article

  const handleDelete = () => {
    deleteArticle(article._id)
  }

  const actions = editable && (
    <Actions className="test--article-actions">
      <Link to={`/articles/${article._id}/edit`}>
        <Icon style={{ fontSize: '20px' }} type="edit" />
      </Link>
      <Popconfirm
        title="Are you sure delete this article?"
        onConfirm={handleDelete}
        okText="Yes"
        cancelText="No"
      >
        <a href="#" data-test="article-delete">
          <Icon style={{ fontSize: '20px' }} type="delete" />
        </a>
      </Popconfirm>
    </Actions>
  )

  return (
    <Card data-test="article-card" title={title} extra={actions}>
      <p data-test="article-meta">
        {date} by {author}
      </p>
      {trimText(content, 210)}
    </Card>
  )
}

const Actions = styled.div`
  display: flex;
  align-items: center;

  a:not(:last-child) {
    margin-right: 10px;
  }
`

ArticleCard.propTypes = {
  editable: PropTypes.bool.isRequired,
  article: PropTypes.shape({
    _id: PropTypes.string.isRequired,
    _v: PropTypes.number,
    title: PropTypes.string.isRequired,
    content: PropTypes.string.isRequired,
    createDate: PropTypes.string.isRequired,
    creator: PropTypes.shape({
      _id: PropTypes.string.isRequired,
      displayName: PropTypes.string.isRequired
    })
  }),
  deleteArticle: PropTypes.func.isRequired
}

export default ArticleCard
