import React, { useEffect } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { fetchArticle, deleteArticle, clearArticle } from '../../actions'
import Loader from '../loader'
import { Button, Popconfirm } from 'antd'
import { history } from '../../history'
import styled from 'styled-components'

export const Article = ({
  user,
  match,
  article,
  receivedAt,
  loading,
  fetchArticle,
  deleteArticle,
  clearArticle
}) => {
  const id = match.params.id

  useEffect(() => {
    fetchArticle(id)

    return () => clearArticle()
    // eslint-disable-next-line
  }, [])

  if (!receivedAt || loading) return <Loader />

  const editable = user && article ? user._id === article.creator._id : false
  const date = new Date(article.createDate).toLocaleDateString()
  const {
    title,
    content,
    creator: { displayName: author }
  } = article

  const handleDelete = () => {
    deleteArticle(article._id)
    history.push('/')
  }

  return (
    <div>
      <h1 data-test="article-title">{title}</h1>
      <Meta>
        {date} by {author}
      </Meta>
      <p data-test="article-content">{content}</p>

      {editable && (
        <Actions data-test="article-actions">
          <Button
            data-test="article-edit"
            type="default"
            onClick={() => history.push(`/articles/${id}/edit`)}
          >
            Edit
          </Button>
          <Popconfirm
            title="Are you sure delete this article?"
            onConfirm={handleDelete}
            okText="Yes"
            cancelText="No"
          >
            <Button type="danger">Delete</Button>
          </Popconfirm>
        </Actions>
      )}
    </div>
  )
}

const Meta = styled.p`
  font-size: 12px;
  margin-bottom: 7px;
`

const Actions = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: center;

  & > * {
    &:not(:last-child) {
      margin-right: 10px;
    }
  }
`

Article.propTypes = {
  article: PropTypes.object,
  user: PropTypes.object,
  loading: PropTypes.bool.isRequired,
  receivedAt: PropTypes.number,
  fetchArticle: PropTypes.func.isRequired,
  deleteArticle: PropTypes.func.isRequired,
  clearArticle: PropTypes.func.isRequired,
  match: PropTypes.object.isRequired
}

export default connect(
  (state) => ({
    article: state.article.data,
    user: state.auth.user,
    loading: state.article.loading,
    receivedAt: state.article.receivedAt
  }),
  { fetchArticle, deleteArticle, clearArticle }
)(Article)
