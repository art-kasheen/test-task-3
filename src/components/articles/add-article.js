import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import { Form, Input, Button } from 'antd'
import { connect } from 'react-redux'
import { addArticle } from '../../actions'

const AddArticle = ({
  form: { getFieldDecorator, validateFields, setFieldsValue },
  addArticle,
  loaded
}) => {
  const [fields, setFields] = useState({ title: '', content: '' })
  const [loading, setLoading] = useState(false)

  useEffect(() => {
    if (loaded) {
      setLoading(false)
    }
  }, [loaded])

  const handleChange = (ev) => {
    setFields({ ...fields, [ev.target.name]: ev.target.value })
    setFieldsValue({ [ev.target.name]: ev.target.value })
  }

  const handleSubmit = (ev) => {
    ev.preventDefault()

    validateFields((err) => {
      if (err) return

      setLoading(true)
      addArticle(fields)
      setFields({ title: '', content: '' })
      setFieldsValue({ title: '', content: '' })
    })
  }

  return (
    <div>
      <>
        <h1>Add article</h1>
        <Form
          onSubmit={handleSubmit}
          className="add-article-form"
          layout="horizontal"
        >
          <Form.Item label="Title">
            {getFieldDecorator('title', {
              rules: [
                { required: true, message: 'Please input title of article!' }
              ]
            })(
              <Input name="title" placeholder="Title" onChange={handleChange} />
            )}
          </Form.Item>
          <Form.Item label="Text">
            {getFieldDecorator('content', {
              rules: [{ required: true, message: 'Please input article text!' }]
            })(
              <Input.TextArea
                name="content"
                type="textarea"
                placeholder="Text"
                onChange={handleChange}
              />
            )}
          </Form.Item>
          <Form.Item>
            <Button type="primary" htmlType="submit" loading={loading}>
              Submit
            </Button>
          </Form.Item>
        </Form>
      </>
    </div>
  )
}

AddArticle.propTypes = {
  form: PropTypes.object.isRequired,
  loaded: PropTypes.bool.isRequired,
  addArticle: PropTypes.func.isRequired
}

const WrappedAddArticle = Form.create({ name: 'add-article' })(AddArticle)

export default connect(
  (state) => ({
    loaded: state.articles.addLoaded
  }),
  { addArticle }
)(WrappedAddArticle)
