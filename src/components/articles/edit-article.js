import React, { useEffect, useState } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { fetchArticle, editArticle } from '../../actions'
import { Form, Input, Button } from 'antd'
import Loader from '../loader'
import { history } from '../../history'

const EditArticle = ({
  article,
  loading,
  receivedAt,
  fetchArticle,
  editArticle,
  match,
  form: { getFieldDecorator, validateFields, setFieldsValue }
}) => {
  const id = match.params.id
  const [fields, setFields] = useState({})
  const [loadingSubmit, setLoadingSubmit] = useState(false)

  useEffect(() => {
    fetchArticle(id)
    // eslint-disable-next-line
  }, [])

  useEffect(() => {
    if (article) {
      setFields({
        title: article.title,
        content: article.content
      })
    }
  }, [article])

  if (!receivedAt || loading) return <Loader />

  const handleChange = (ev) => {
    setFields({ ...fields, [ev.target.name]: ev.target.value })
    setFieldsValue({ [ev.target.name]: ev.target.value })
  }

  const handleSubmit = (ev) => {
    ev.preventDefault()

    validateFields((err) => {
      if (err) return

      setLoadingSubmit(true)
      editArticle(id, fields)
    })
  }

  return (
    <>
      <h1>Edit article</h1>
      <Form
        onSubmit={handleSubmit}
        className="add-article-form"
        layout="horizontal"
      >
        <Form.Item label="Title">
          {getFieldDecorator('title', {
            rules: [
              { required: true, message: 'Please input title of article!' }
            ],
            initialValue: fields.title
          })(
            <Input name="title" placeholder="Title" onChange={handleChange} />
          )}
        </Form.Item>
        <Form.Item label="Text">
          {getFieldDecorator('content', {
            rules: [{ required: true, message: 'Please input article text!' }],
            initialValue: fields.content
          })(
            <Input.TextArea
              name="content"
              type="textarea"
              placeholder="Text"
              onChange={handleChange}
            />
          )}
        </Form.Item>
        <Form.Item>
          <Button
            type="primary"
            loading={loadingSubmit}
            htmlType="submit"
            style={{ marginRight: '10px' }}
          >
            Save
          </Button>
          <Button
            type="default"
            onClick={() => history.push(`/articles/${id}`)}
          >
            Cancel
          </Button>
        </Form.Item>
      </Form>
    </>
  )
}

EditArticle.propTypes = {
  match: PropTypes.object.isRequired,
  article: PropTypes.object,
  loading: PropTypes.bool.isRequired,
  receivedAt: PropTypes.number,
  form: PropTypes.object.isRequired,
  fetchArticle: PropTypes.func.isRequired,
  editArticle: PropTypes.func.isRequired
}

const WrappedEditArticle = Form.create({ name: 'edit-article' })(EditArticle)

export default connect(
  (state) => ({
    article: state.article.data,
    loading: state.article.loading,
    receivedAt: state.article.receivedAt
  }),
  { fetchArticle, editArticle }
)(WrappedEditArticle)
