import React from 'react'
import { Link } from 'react-router-dom'
import styled from 'styled-components'

const Menu = () => {
  return (
    <List>
      <li>
        <Link to="/">Home</Link>
      </li>
    </List>
  )
}

const List = styled.ul`
  margin: 0;
  padding: 0;
  list-style-type: none;
`

Menu.propTypes = {}

export default Menu
