import React from 'react'
import PropTypes from 'prop-types'
import { Switch, Route } from 'react-router-dom'
import SignInForm from '../auth/sign-in-form'
import SignUpForm from '../auth/sign-up-form'
import { SmallContainer } from '../../style/layout'
import { connect } from 'react-redux'
import { signUp, signIn, googleSignIn } from '../../actions'

const Auth = ({ signUp, signIn, googleSignIn }) => {
  const handleSignIn = (value) => signIn(value)
  const handleSignUp = (value) => signUp(value)

  return (
    <SmallContainer>
      <Switch>
        <Route
          exact
          path="/auth/sign-in"
          render={() => (
            <SignInForm onSubmit={handleSignIn} googleSignIn={googleSignIn} />
          )}
        />
        <Route
          exact
          path="/auth/sign-up"
          render={() => <SignUpForm onSubmit={handleSignUp} />}
        />
      </Switch>
    </SmallContainer>
  )
}

Auth.propTypes = {
  googleSignIn: PropTypes.func.isRequired,
  signIn: PropTypes.func.isRequired,
  signUp: PropTypes.func.isRequired
}

export default connect(
  null,
  { signUp, signIn, googleSignIn }
)(Auth)
