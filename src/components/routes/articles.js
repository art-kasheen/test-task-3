import React from 'react'
import PropTypes from 'prop-types'
import { Switch, Route } from 'react-router-dom'
import PrivateRoute from '../private-route'
import AddArticle from '../articles/add-article'
import ArticleList from '../articles/article-list'
import EditArticle from '../articles/edit-article'
import Article from '../articles/article'
import { Container } from '../../style/layout'

const Articles = ({ isAuthenticated }) => {
  return (
    <Container>
      <Switch>
        <Route path="/articles" exact render={() => <ArticleList />} />
        <PrivateRoute
          path="/articles/new"
          exact
          isAuthenticated={isAuthenticated}
          component={AddArticle}
        />
        <Route path="/articles/:id" exact component={Article} />
        <PrivateRoute
          path="/articles/:id/edit"
          exact
          isAuthenticated={isAuthenticated}
          component={EditArticle}
        />
      </Switch>
    </Container>
  )
}

Articles.propTypes = {
  isAuthenticated: PropTypes.bool.isRequired
}

export default Articles
