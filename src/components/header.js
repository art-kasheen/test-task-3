import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { Container } from '../style/layout'
import Menu from './menu'
import { Button } from 'antd'
import { history } from '../history'
import { connect } from 'react-redux'
import { signOut } from '../actions'

const Header = ({ user, signOut }) => {
  return (
    <Container>
      <Row>
        <Menu />
        <UserLine>
          {user && (
            <>
              <UserName>Hi, {user.username || user.name}!</UserName>
              <Button
                type="default"
                onClick={() => history.push('/articles/new')}
              >
                Add article
              </Button>
            </>
          )}
          {user ? (
            <Button
              type="primary"
              onClick={() => {
                signOut()
              }}
            >
              Sign Out
            </Button>
          ) : (
            <Button
              type="primary"
              onClick={() => history.push('/auth/sign-in')}
            >
              Sign In
            </Button>
          )}
        </UserLine>
      </Row>
    </Container>
  )
}

Header.propTypes = {
  user: PropTypes.object,
  signOut: PropTypes.func.isRequired
}

const Row = styled.div`
  display: flex;
  flex-wrap: wrap;
  align-items: center;
  justify-content: space-between;
  padding: 15px 30px;
  background: #f1f3f4;
  margin-bottom: 20px;

  @media screen and (max-width: 480px) {
    flex-direction: column;
  }
`

const UserLine = styled.div`
  display: flex;
  flex-wrap: wrap;
  align-items: center;

  & > * {
    :not(:last-child) {
      margin: 0 15px 0 0;
    }

    :first-child {
      @media screen and (max-width: 480px) {
        margin: 15px 0;
      }
    }
  }
`

const UserName = styled.p`
  font-size: 13px;

  @media screen and (max-width: 480px) {
    width: 100%;
    text-align: center;
  }
`

export default connect(
  (state) => ({
    user: state.auth.user
  }),
  { signOut }
)(Header)
