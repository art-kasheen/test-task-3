import React from 'react'
import PropTypes from 'prop-types'
import { Spin } from 'antd'
import styled from 'styled-components'

const Loader = ({ size = 'default' }) => {
  return (
    <Wrapper>
      <Spin size={size} />
    </Wrapper>
  )
}

const Wrapper = styled.div`
  margin: 30px auto;
  width: 40px;
  height: 40px;
`

Loader.propTypes = {
  size: PropTypes.string
}

export default Loader
