import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { Form, Icon, Input, Button } from 'antd'
import styled from 'styled-components'
import { history } from '../../history'

export const SignInForm = ({
  form: { getFieldDecorator, validateFields, setFieldsValue },
  googleSignIn,
  onSubmit
}) => {
  const [fields, setFields] = useState({ username: '', password: '' })

  const handleChange = (ev) => {
    setFields({ ...fields, [ev.target.name]: ev.target.value })
    setFieldsValue(fields)
  }

  const handleSubmit = (ev) => {
    ev.preventDefault()

    validateFields((err) => {
      if (err) return

      onSubmit({ ...fields })
      setFields({ username: '', password: '' })
      setFieldsValue(fields)
    })
  }

  return (
    <>
      <h1>Sign In</h1>
      <Form onSubmit={handleSubmit} className="login-form">
        <Form.Item>
          {getFieldDecorator('username', {
            rules: [{ required: true, message: 'Please input your username!' }]
          })(
            <Input
              data-test="username-field"
              prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
              name="username"
              placeholder="Username"
              onChange={handleChange}
            />
          )}
        </Form.Item>
        <Form.Item>
          {getFieldDecorator('password', {
            rules: [{ required: true, message: 'Please input your Password!' }]
          })(
            <Input
              data-test="password-field"
              prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
              name="password"
              type="password"
              placeholder="Password"
              onChange={handleChange}
            />
          )}
        </Form.Item>
        <Form.Item>
          <Button
            type="primary"
            htmlType="submit"
            className="login-form-button"
          >
            Sign In
          </Button>
        </Form.Item>

        <GoogleButton>
          <Button type="link" onClick={() => history.push('/auth/sign-up')}>
            No account yet ? Please Sign Up!
          </Button>

          <Button type="dashed" onClick={googleSignIn}>
            Sign In with Google
          </Button>
        </GoogleButton>
      </Form>
    </>
  )
}

const GoogleButton = styled.div`
  text-align: center;
`

SignInForm.propTypes = {
  googleSignIn: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired
}

export const WrappedSignInForm = Form.create({ name: 'sign-in' })(SignInForm)

export default WrappedSignInForm
