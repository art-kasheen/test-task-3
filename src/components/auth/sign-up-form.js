import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { Form, Icon, Input, Button } from 'antd'
import ReCAPTCHA from 'react-google-recaptcha'
import { history } from '../../history'

const SignUpForm = ({
  form: { getFieldDecorator, setFieldsValue, validateFields },
  onSubmit
}) => {
  const [fields, setFields] = useState({ username: '', password: '' })
  const [captcha, setCaptcha] = useState('')
  const recaptchaRef = React.createRef()

  const verifyCaptcha = (token) => {
    setCaptcha(token)
  }

  const handleChange = (ev) => {
    setFields({ ...fields, [ev.target.name]: ev.target.value })
    setFieldsValue(fields)
  }

  const handleSubmit = (ev) => {
    ev.preventDefault()

    validateFields((err) => {
      if (err) return

      if (captcha) {
        onSubmit({ ...fields, 'g-recaptcha-response': captcha })
        recaptchaRef.current.reset()
        setCaptcha('')
        setFields({ username: '', password: '' })
        setFieldsValue(fields)
      }
    })
  }

  return (
    <>
      <h1>Sign Up</h1>
      <Form onSubmit={handleSubmit} className="login-form">
        <Form.Item>
          {getFieldDecorator('username', {
            rules: [{ required: true, message: 'Please input your username!' }]
          })(
            <Input
              prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
              placeholder="Username"
              name="username"
              onChange={handleChange}
            />
          )}
        </Form.Item>
        <Form.Item>
          {getFieldDecorator('password', {
            rules: [
              { required: true, message: 'Please input your Password!' },
              { min: 4, message: 'Too short!' }
            ]
          })(
            <Input
              prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
              type="password"
              placeholder="Password"
              name="password"
              onChange={handleChange}
            />
          )}
        </Form.Item>
        <ReCAPTCHA
          ref={recaptchaRef}
          sitekey="6LfwpbEUAAAAABhfJcHgtRVSkgsrzRFIVnufwbt4"
          onChange={verifyCaptcha}
        />
        <Form.Item>
          <Button
            type="primary"
            htmlType="submit"
            className="login-form-button"
          >
            Sign Up
          </Button>
        </Form.Item>
        <Button type="link" onClick={() => history.push('/auth/sign-in')}>
          Already have an account ? Please Sign In
        </Button>
      </Form>
    </>
  )
}

SignUpForm.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  form: PropTypes.object
}

const WrappedSignUpForm = Form.create({ name: 'sign-up' })(SignUpForm)

export default WrappedSignUpForm
