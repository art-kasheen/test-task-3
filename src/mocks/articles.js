const data = {
  feeds: [
    {
      _id: '5d4b078bcc239d9956017b4a',
      title: 'titlehjkhuukhkh ,m  jhiuh iuh k',
      content: 'text',
      creator: { _id: '5d4acd6911420398763760ea', displayName: 'user1' },
      __v: 0,
      createDate: '2019-08-07T17:16:59.367Z'
    },
    {
      _id: '5d4b07cacc239d9956017b4b',
      title: 'article',
      content: 'article text',
      creator: { _id: '5d4acd6911420398763760ea', displayName: 'user1' },
      __v: 0,
      createDate: '2019-08-07T17:18:02.084Z'
    },
    {
      _id: '5d4be323cc239d9956017b51',
      title: 'title',
      content: 'text',
      creator: { _id: '5d4ad23ccc239d9956017b47', displayName: 'user2' },
      __v: 0,
      createDate: '2019-08-08T08:53:55.231Z'
    },
    {
      _id: '5d4d3a6ccc239d9956017b52',
      title: 'title 2',
      content: '1234567',
      creator: { _id: '5d4acd6911420398763760ea', displayName: 'user1' },
      __v: 0,
      createDate: '2019-08-09T09:18:36.397Z'
    },
    {
      _id: '5d4d3cefcc239d9956017b56',
      title: 'newww',
      content: 'texter',
      creator: { _id: '5d4ad23ccc239d9956017b47', displayName: 'user2' },
      __v: 0,
      createDate: '2019-08-09T09:29:19.574Z'
    },
    {
      _id: '5d4de2b2cc239d9956017b57',
      title: 'title',
      content: '1234567',
      creator: { _id: '5d4acd6911420398763760ea', displayName: 'user1' },
      __v: 0,
      createDate: '2019-08-09T21:16:34.031Z'
    },
    {
      _id: '5d4de42dcc239d9956017b58',
      title: 'title 232',
      content: 'text',
      creator: {
        _id: '5d49b20b54f69419917c3bea',
        displayName: 'Artem Tapolsky'
      },
      __v: 0,
      createDate: '2019-08-09T21:22:53.051Z'
    },
    {
      _id: '5d504874cc239d9956017b59',
      title:
        'Некоторые версии появились по ошибке, некоторые - намеренно (например, юмористические варианты).',
      content:
        'Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при простой дубликации "Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст.." Многие программы электронной вёрстки и редакторы HTML используют Lorem Ipsum в качестве текста по умолчанию, так что поиск по ключевым словам "lorem ipsum" сразу показывает, как много веб-страниц всё ещё дожидаются своего настоящего рождения. За прошедшие годы текст Lorem Ipsum получил много версий. Некоторые версии появились по ошибке, некоторые - намеренно (например, юмористические варианты).',
      creator: { _id: '5d4acd6911420398763760ea', displayName: 'user1' },
      __v: 0,
      createDate: '2019-08-11T16:55:16.600Z'
    }
  ]
}

export default data
