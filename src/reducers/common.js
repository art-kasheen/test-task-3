import { produce } from 'immer'
import { INIT_START, INIT_SUCCESS } from '../constants'

export const initialState = {
  loading: false,
  initialized: false,
  errors: [],
  messages: []
}

export default (state = initialState, action) => {
  const { type, payload } = action

  return produce(state, (draft) => {
    switch (type) {
      case INIT_START:
        draft.loading = true
        break

      case INIT_SUCCESS:
        draft.loading = false
        draft.initialized = true
        break

      default:
        return
    }
  })
}
