import { produce } from 'immer'
import {
  ADD_ARTICLE_REQUEST,
  ADD_ARTICLE_SUCCESS,
  FETCH_ARTICLES_SUCCESS,
  FETCH_ARTICLES_REQUEST,
  DELETE_ARTICLE_SUCCESS
} from '../constants'

export const initialState = {
  data: [],
  loading: false,
  receivedAt: null,
  addLoaded: false
}

export default (state = initialState, action) => {
  const { type, payload } = action

  return produce(state, (draft) => {
    switch (type) {
      case ADD_ARTICLE_REQUEST:
        draft.addLoaded = false
        break

      case ADD_ARTICLE_SUCCESS:
        draft.addLoaded = true
        break

      case DELETE_ARTICLE_SUCCESS:
        draft.data.splice(
          draft.data.findIndex((article) => article._id === payload._id),
          1
        )
        break

      case FETCH_ARTICLES_REQUEST:
        draft.loading = true
        break

      case FETCH_ARTICLES_SUCCESS:
        draft.loading = false
        draft.receivedAt = Date.now()
        draft.data = payload.feeds
        break

      default:
        return
    }
  })
}
