import { combineReducers } from 'redux'
import articles from './articles'
import article from './article'
import auth from './auth'
import common from './common'

export default combineReducers({
  articles,
  article,
  auth,
  common
})
