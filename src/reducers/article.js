import { produce } from 'immer'
import {
  FETCH_ARTICLE_REQUEST,
  FETCH_ARTICLE_SUCCESS,
  EDIT_ARTICLE_SUCCESS,
  CLEAR_ARTICLE
} from '../constants'

export const initialState = {
  data: null,
  loading: false,
  receivedAt: null
}

export default (state = initialState, action) => {
  const { type, payload } = action

  return produce(state, (draft) => {
    switch (type) {
      case CLEAR_ARTICLE:
        draft.data = null
        draft.receivedAt = null
        break

      case FETCH_ARTICLE_REQUEST:
        draft.loading = true
        break

      case FETCH_ARTICLE_SUCCESS:
        draft.loading = false
        draft.data = payload.feed
        draft.receivedAt = Date.now()
        break

      case EDIT_ARTICLE_SUCCESS:
        draft.data = payload.feed
        break

      default:
        return
    }
  })
}
