import { produce } from 'immer'
import {
  SIGN_IN_SUCCESS,
  SIGN_UP_SUCCESS,
  SIGN_UP_REQUEST,
  SIGN_IN_REQUEST,
  SIGN_OUT_SUCCESS,
  SIGN_OUT_REQUEST
} from '../constants'

export const initialState = {
  authToken: null,
  user: null,
  isAuthenticate: false,
  loading: true
}

export default (state = initialState, action) => {
  const { type, payload } = action

  return produce(state, (draft) => {
    switch (type) {
      case SIGN_UP_REQUEST:
      case SIGN_IN_REQUEST:
      case SIGN_OUT_REQUEST:
        draft.loading = true
        break

      case SIGN_UP_SUCCESS:
        draft.authToken = payload.token
        draft.loading = false
        break

      case SIGN_IN_SUCCESS:
        draft.user = payload.user
        draft.loading = false
        break

      case SIGN_OUT_SUCCESS:
        draft.user = null
        draft.loading = false
        break

      default:
        return
    }
  })
}
