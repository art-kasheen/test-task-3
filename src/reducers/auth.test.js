import reducer, { initialState } from './auth'
import {
  SIGN_IN_REQUEST,
  SIGN_IN_SUCCESS,
  SIGN_OUT_SUCCESS,
  SIGN_UP_SUCCESS
} from '../constants'
import user from '../mocks/user'

describe('auth test', () => {
  it('SIGN_IN_REQUEST', () => {
    const action = {
      type: SIGN_IN_REQUEST
    }

    const expectedState = reducer(initialState, action)

    expect({
      ...initialState,
      loading: true
    }).toEqual(expectedState)
  })

  it('SIGN_UP_SUCCESS', () => {
    const action = {
      type: SIGN_UP_SUCCESS,
      payload: {
        token:
          'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE1NjYyMjc2MTUsImlkIjoiNWQ0YWNkNjkxMTQyMDM5ODc2Mzc2MGVhIiwiaWF0IjoxNTY2MjI0MDE1fQ.YpVnmSuX86MVVVdhrAwxWdslQkVNj999Ra_D7ZasxhY'
      }
    }

    const prevState = {
      ...initialState,
      loading: true
    }

    const expectedState = reducer(prevState, action)

    expect({
      ...prevState,
      loading: false,
      authToken: action.payload.token
    }).toEqual(expectedState)
  })

  it('SIGN_IN_SUCCESS', () => {
    const action = {
      type: SIGN_IN_SUCCESS,
      payload: { user }
    }

    const prevState = {
      ...initialState,
      loading: true
    }

    const expectedState = reducer(prevState, action)

    expect({
      ...prevState,
      loading: false,
      user: action.payload.user
    }).toEqual(expectedState)
  })

  it('SIGN_OUT_SUCCESS', () => {
    const action = {
      type: SIGN_OUT_SUCCESS
    }

    const prevState = {
      ...initialState,
      user
    }

    const expectedState = reducer(prevState, action)

    expect({
      ...prevState,
      user: null,
      loading: false
    }).toEqual(expectedState)
  })
})
