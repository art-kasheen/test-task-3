import reducer, { initialState } from './articles'
import {
  ADD_ARTICLE_REQUEST,
  ADD_ARTICLE_SUCCESS,
  FETCH_ARTICLES_REQUEST,
  FETCH_ARTICLES_SUCCESS,
  DELETE_ARTICLE_SUCCESS
} from '../constants'
import articles from '../mocks/articles'

describe('articles reducer', () => {
  it('ADD_ARTICLE_REQUEST', () => {
    const action = {
      type: ADD_ARTICLE_REQUEST
    }

    const expectedState = reducer(initialState, action)

    expect({
      ...initialState,
      addLoaded: false
    }).toEqual(expectedState)
  })

  it('ADD_ARTICLE_SUCCESS', () => {
    const action = {
      type: ADD_ARTICLE_SUCCESS
    }

    const expectedState = reducer(initialState, action)

    expect({
      ...initialState,
      addLoaded: true
    }).toEqual(expectedState)
  })

  it('FETCH_ARTICLES_REQUEST', () => {
    const action = {
      type: FETCH_ARTICLES_REQUEST
    }

    const expectedState = reducer(initialState, action)

    expect({
      ...initialState,
      loading: true
    }).toEqual(expectedState)
  })

  it('FETCH_ARTICLES_SUCCESS', () => {
    const action = {
      type: FETCH_ARTICLES_SUCCESS,
      payload: articles
    }

    const prevState = {
      ...initialState,
      loading: true
    }

    const expectedState = reducer(prevState, action)

    expect({
      ...prevState,
      data: articles.feeds,
      loading: false,
      receivedAt: expectedState.receivedAt
    }).toEqual(expectedState)
  })

  it('DELETE_ARTICLE_SUCCESS', () => {
    const action = {
      type: DELETE_ARTICLE_SUCCESS,
      payload: {
        _id: '5d4b078bcc239d9956017b4a'
      }
    }

    const prevState = {
      ...initialState,
      data: articles.feeds,
      receivedAt: Date.now()
    }

    const expectedState = reducer(prevState, action)

    expect({
      ...prevState,
      data: articles.feeds.filter(
        (article) => article._id !== action.payload._id
      )
    }).toEqual(expectedState)
  })
})
