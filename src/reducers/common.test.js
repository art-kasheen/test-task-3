import reducer, { initialState } from './common'
import { INIT_START, INIT_SUCCESS } from '../constants'

describe('common reducer', () => {
  it('INIT_START', () => {
    const action = {
      type: INIT_START
    }

    const expectedState = reducer(initialState, action)

    expect({
      ...initialState,
      loading: true
    }).toEqual(expectedState)
  })

  it('INIT_SUCCESS', () => {
    const action = {
      type: INIT_SUCCESS
    }

    const prevState = {
      ...initialState,
      loading: true
    }

    const expectedState = reducer(prevState, action)

    expect({
      ...prevState,
      loading: false,
      initialized: true
    }).toEqual(expectedState)
  })
})
