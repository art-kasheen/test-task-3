import {
  FETCH_ARTICLE_SUCCESS,
  FETCH_ARTICLE_REQUEST,
  CLEAR_ARTICLE,
  EDIT_ARTICLE_SUCCESS
} from '../constants'
import reducer, { initialState } from '../reducers/article'
import articles from '../mocks/articles'

describe('article reducer', () => {
  const article = articles.feeds[0]

  it('CLEAR_ARTICLE', () => {
    const prevState = {
      ...initialState,
      data: article,
      receivedAt: Date.now()
    }

    const action = {
      type: CLEAR_ARTICLE
    }

    const expectedState = reducer(prevState, action)

    expect({
      ...prevState,
      data: null,
      receivedAt: null
    }).toEqual(expectedState)
  })

  it('FETCH_ARTICLE_REQUEST', () => {
    const action = {
      type: FETCH_ARTICLE_REQUEST
    }

    const expectedState = reducer(initialState, action)

    expect({
      ...initialState,
      loading: true
    }).toEqual(expectedState)
  })

  it('FETCH_ARTICLE_SUCCESS', () => {
    const prevState = {
      ...initialState,
      loading: true
    }

    const action = {
      type: FETCH_ARTICLE_SUCCESS,
      payload: {
        feed: article
      }
    }

    const expectedState = reducer(prevState, action)

    expect({
      ...prevState,
      loading: false,
      receivedAt: expectedState.receivedAt,
      data: article
    }).toEqual(expectedState)
  })

  it('EDIT_ARTICLE_SUCCESS', () => {
    const action = {
      type: EDIT_ARTICLE_SUCCESS,
      payload: {
        feed: articles.feeds[2]
      }
    }

    const prevState = {
      ...initialState,
      data: article,
      receivedAt: Date.now()
    }

    const expectedState = reducer(prevState, action)

    expect({
      ...prevState,
      data: articles.feeds[2]
    }).toEqual(expectedState)
  })
})
