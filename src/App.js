import React from 'react'
import { Provider } from 'react-redux'
import { Router } from 'react-router'
import store from './store'
import { history } from './history'
import Root from './components/root'

function App() {
  return (
    <Provider store={store}>
      <Router history={history}>
        <Root />
      </Router>
    </Provider>
  )
}

export default App
